# TMDBClient

A simple Android client for The Movie DB in Material Design


## Screenshots

![alt text](https://lh3.googleusercontent.com/Lzm9VSHQ8OnIzxiY27zeuF055lf0sLbgdm3OWua8BxOErTeWw-vtDib-slv1Lt6TYdf3MPu7_ep6_4tYRtTZvzG73CQc9RrlzZzLIjYkom9pen7WJdoiMY2wX2eumwoYO1V7rATJyPGddrv7ymUqe72DOOIIxge-JgIEPCHk8FW3WGxpzHfTA4Kmlv3nJ5Sjq6rggDQ66_OrvgGxiME8Li9Cl9ZpLzRdXRN1nsm4rFCoH-N2HfEI8wt76zmb3PRDvQXktdu7Y2d1OMVKHysNN47zp6RnOSl_w8yJ1lrEg4gGcuEy7XTUwcMYDK1i-W7EsimN9GFLzj2Ry1kA23jKy0p4Veh1SkIOaE6gQ1rHSWi95XpUDU4COBuQ6tTc-c_OXmAvM-TU0HKaSuUwMhFCJq_Tz7HwIqYHHHKUMoj3r-CK5SXirGlQ7nIQWI_C-M5VrhL5fkMLXKvuxCprMJ_w5___d0V9OBpKcsEB9nUma2M0Q0t14oeAfTMhHvOXgQtDNUbuCmG2dpqiBLT0CaHT07m4KHQnp9DP3TVRNtuO66cdpik2oOkuIbl-xqIPiN09iTyuJw6sFEaGgusCjHTKOW8YoQLZRvMjW_L3MGx1Ir_XR5od536EfVp2frB4RSXafkOhQgi40S6nREaCj6hgMNWoVX2dO7Tt=w1920-h1006-no)

![alt text](https://lh3.googleusercontent.com/NpuH0z000mksA12axdCc3GgwZ1So8zBG2LLMxBJ0PCTeSPF6i7OmC9v_8WvlFfiaRZ41ZT3CE2dGXKb2uTZ92fqAI2UGqn6iuJfgJerkg2j0kikM6wZbFvyJ4pHaijOzu1BUumbhgL9nN7FxRJxqE-H_tN2YkQV7Ot4KmujZ6cjwQO_yPHb4TS0xoeLes-NMdFSqxdXvzDWHOFMPWwiQ-5SCoivUk8vWT89TBPK9eIft8kkz_51sEF4yA1b-s7eTbhzc1t-efyvlAp15SSYy_PFLJwbrLeXyDJBBT_mZSEGUE4BBDkstfVDqbz4XIVVJcph6uuyKRzzXzxmnqTLc46pG1R9pac6sT0AAcudSy48SiQ3RBFTPx4_Y-qpPN2xzZbOLjmJFqRyr8bOIzQVf3fsvRiaK_WwgHFyHRQOQlcGm9HPY5Yu1AzWvozTCkZuIP6SGJpJhDHGi9B1QNDYZrtOPAhqhKsEvOKkzw3hFqf0fCyE37oEn-hAsbbdBsDRhJ4iI7OlDP-rvdr7xXjPvEBozV2LeUVJR8mROy7JKiCKjjsmqCbtPSifTkahM2LIHqU_yBqSq4U_eX4g4OcEOh6vIxABVIxsrdXY0ho6b9aVA2xklaDvH4NkqhSvMKUyTpWEHAsXiGNFTJ4lkxrwsIZ_WeE7QsWPb=w820-h1400-no)

## Architecture

VIPER architecture approach imposes discipline on indirect transfer of control, don't force users of a component to depend on things they don't need, improves testability, gives independency from the frameworks and UI, enforce granularity.

![alt text](https://lh3.googleusercontent.com/2bal_FtHWYqnjGK5HeSHjY2CCzLAxhDqZaGzRprIfcZqCOQdOj1WA2yArwH_8P9pAReQMB0YG6EQj8I7uJDxaI64Nrk50yXFTuIiputw3yIBYGt758ByQVaNXDfu3iZ1_iNgMq95JwBH00WUZayWddO_C892pS9otYqAPw4VCw8fEK4NtTi7qizCCl_gSemaPuNt_YjdyImHR0SUResAxcdDL5zyCsUmi5ri22Cz8pLToYRu4Krl45q9ytWVbS055YOmJoSMfd5QA5vSXnOE9rGieZuS4kUSHce4QtpHOZ-F4yEgH_38VQSqCe6jrvHLpzVJ8abzsjizr49VMQpQrAoFfXC24PWQsuxNsCUbds1IVhF_Vduc4nN0UYNIYMOEwKxAJ02Dre7jORn6PAce0hA7hgcdxoMvnRTml7PMLpNnOzKN5YGz1VDlYirvlFooVLWXNLJOtKcHwQbch_zzdmhFH-l-3I4DpC2n1wcwJV7hB4qvAve4pTKc_H0BAb755NmIVK0FP4-incqBCR0HnshkGPB2mvEdIYEeyZbdTN1lRFnCUbkqo63zgNZrUN0F-DleEIZVV-f6rZjLZQ5QC7hZgH5SF6hIe4i_0cvaRW5_sKc-UL5AsYXEZ2zY2wWP_ByeCcOybgkkA3tB8ASoj3iMP2I9fG7q=w601-h481-no)

### View 
Responsible for the observing and rendering of the current state of the system to user and deliver user events(such as clicks) to the ViewModel.

### ViewModel 
Responsible for the handling events from the View and for the dispatching all needed commands for the updating UI state. Also responsible for the dispatching routing commands to the Router and for the listening of the updates from the Interactor.

### Router 
Responsible for the dispatching the navigation commands to particular target to the Navigator.

### Navigator 
Responsible for the processing incoming navigation commands and dispatching them to the Framework.


### Interactor 
Responsible for the keeping all needed business logic and it get all information from the Repository.

### Repository 
Responsible for the encapsulating the set of objects persisted in a data store and the operations performed over them, providing a more object-oriented view of the persistence layer.

## Third-party dependencies
DI - Dagger 2

Network - Retrofit

ReactiveX - RxJava, RxAndroid

Images - Glide

Logging - Timber
