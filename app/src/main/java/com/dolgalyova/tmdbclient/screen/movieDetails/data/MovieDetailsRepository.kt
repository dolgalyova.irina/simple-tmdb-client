package com.dolgalyova.tmdbclient.screen.movieDetails.data

import com.dolgalyova.tmdbclient.screen.movieDetails.data.model.Movie
import io.reactivex.Single

interface MovieDetailsRepository {
    fun getMovie(id: Int): Single<Movie>
}