package com.dolgalyova.tmdbclient.screen.movieList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dolgalyova.tmdbclient.BuildConfig
import com.dolgalyova.tmdbclient.R
import com.dolgalyova.tmdbclient.common.util.getInteger
import com.dolgalyova.tmdbclient.common.util.setLoadMoreListener
import com.dolgalyova.tmdbclient.common.util.showGeneralError
import com.dolgalyova.tmdbclient.screen.movieList.di.MovieListComponent
import com.dolgalyova.tmdbclient.screen.movieList.presentation.MovieListType
import com.dolgalyova.tmdbclient.screen.movieList.presentation.MovieListViewModel
import com.dolgalyova.tmdbclient.screen.movieList.presentation.MovieListViewModelFactory
import com.dolgalyova.tmdbclient.screen.movieList.view.MovieListAdapter
import kotlinx.android.synthetic.main.fragment_movie_list.*
import javax.inject.Inject

class MovieListFragment : Fragment() {

    companion object {
        private const val EXTRA_MOVIE_LIST_TYPE = "${BuildConfig.APPLICATION_ID}.EXTRA_MOVIE_LIST_TYPE"

        fun create(type: MovieListType): MovieListFragment {
            val args = Bundle()
                    .apply { putInt(EXTRA_MOVIE_LIST_TYPE, type.ordinal) }
            return MovieListFragment().apply { arguments = args }
        }
    }

    private val component by lazy {
        val type = MovieListType.values()[arguments?.getInt(EXTRA_MOVIE_LIST_TYPE) ?: 0]
        (parentFragment as MovieListComponent.ComponentProvider).provideMovieListComponent(type)
    }
    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MovieListViewModel::class.java)
    }
    @Inject lateinit var viewModelFactory: MovieListViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_movie_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()

        viewModel.itemsObservable.observe(this,
                Observer { (moviesList.adapter as MovieListAdapter).submitList(it) })
        viewModel.generalErrorObservable.observe(this, Observer { showGeneralError() })
    }

    private fun initViews() {
        val columnCount = getInteger(R.integer.grid_column_count)
        moviesList.layoutManager = GridLayoutManager(context, columnCount)
                .apply { orientation = RecyclerView.VERTICAL }
        moviesList.itemAnimator = DefaultItemAnimator()
        moviesList.adapter = MovieListAdapter(viewModel::onMovieClick)
        moviesList.setLoadMoreListener(viewModel::onLoadMore)
    }
}