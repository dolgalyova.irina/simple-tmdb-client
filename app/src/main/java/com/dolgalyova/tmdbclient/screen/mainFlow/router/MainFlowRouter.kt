package com.dolgalyova.tmdbclient.screen.mainFlow.router

import com.dolgalyova.tmdbclient.screen.movieList.data.model.MovieShort

interface MainFlowRouter {

    fun openMovie(movie: MovieShort)

    fun goBack()
}