package com.dolgalyova.tmdbclient.screen.movieDetails.di

import com.dolgalyova.tmdbclient.common.di.FlowScope
import com.dolgalyova.tmdbclient.screen.movieDetails.MovieDetailsFragment
import com.dolgalyova.tmdbclient.screen.movieDetails.domain.MovieId
import dagger.BindsInstance
import dagger.Subcomponent

@FlowScope
@Subcomponent(modules = [MovieDetailsModule::class])
interface MovieDetailsComponent {

    fun inject(target: MovieDetailsFragment)

    @Subcomponent.Builder
    interface Builder {
        fun module(module: MovieDetailsModule): Builder
        @BindsInstance fun movieId(@MovieIdQualifier movieId: Int): Builder
        fun build(): MovieDetailsComponent
    }

    interface ComponentProvider {
        fun provideMovieDetailsComponent(id: MovieId): MovieDetailsComponent
    }
}