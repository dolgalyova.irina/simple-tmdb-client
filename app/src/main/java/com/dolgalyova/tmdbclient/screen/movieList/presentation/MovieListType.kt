package com.dolgalyova.tmdbclient.screen.movieList.presentation

enum class MovieListType {
    Popular, Upcoming, NowPlaying
}