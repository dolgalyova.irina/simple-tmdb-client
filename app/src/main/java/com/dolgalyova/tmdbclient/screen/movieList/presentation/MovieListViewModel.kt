package com.dolgalyova.tmdbclient.screen.movieList.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.tmdbclient.common.arch.SingleLiveEvent
import com.dolgalyova.tmdbclient.screen.movieList.data.model.MovieShort
import com.dolgalyova.tmdbclient.screen.movieList.domain.MovieListInteractor
import com.dolgalyova.tmdbclient.screen.movieList.router.MovieListRouter
import timber.log.Timber

class MovieListViewModel(private val interactor: MovieListInteractor,
                         private val router: MovieListRouter) : ViewModel() {
    private var isLoading = false
        set(value) {
            field = value
            val currentItems = items.value.orEmpty()
            if (field) {
                val loading = if (currentItems.isEmpty()) MovieListItem.InitialProgress
                else MovieListItem.LoadNextProgress

                items.value = currentItems + loading
            } else {
                items.value = currentItems.filter { it is MovieListItem.Movie }
            }
        }
    private var page: Int = 0
    private val items = MutableLiveData<List<MovieListItem>>()
    val itemsObservable: LiveData<List<MovieListItem>>
        get() = items
    private val errors = SingleLiveEvent<Any>()
    val generalErrorObservable: LiveData<Any>
        get() = errors

    init {
        loadNextPage()
    }

    override fun onCleared() {
        super.onCleared()
        interactor.cancel()
    }

    fun onMovieClick(movie: MovieShort) = router.openMovie(movie)

    fun onLoadMore() = loadNextPage()

    private fun loadNextPage() {
        if (isLoading) return
        isLoading = true
        page += 1
        interactor.getMovies(page, ::onPageAvailable, ::onError)
    }

    private fun onPageAvailable(page: List<MovieShort>) {
        val currentItems = items.value.orEmpty()
        val currentMovies = currentItems.filter { it is MovieListItem.Movie }
        val updatedMovies = currentMovies + page.map { MovieListItem.Movie(it) }
        items.value = updatedMovies
        isLoading = false
    }

    private fun onError(error: Throwable) {
        page -= 1
        isLoading = false
        Timber.e(error)
        errors.call()
    }
}

class MovieListViewModelFactory(private val interactor: MovieListInteractor,
                                private val router: MovieListRouter) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T = MovieListViewModel(interactor, router) as T
}