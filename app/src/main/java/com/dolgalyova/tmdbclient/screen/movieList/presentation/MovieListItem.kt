package com.dolgalyova.tmdbclient.screen.movieList.presentation

import com.dolgalyova.tmdbclient.screen.movieList.data.model.MovieShort

sealed class MovieListItem {
    object InitialProgress : MovieListItem()
    object LoadNextProgress : MovieListItem()
    class Movie(val item: MovieShort) : MovieListItem()
}