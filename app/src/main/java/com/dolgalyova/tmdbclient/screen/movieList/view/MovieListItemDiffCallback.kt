package com.dolgalyova.tmdbclient.screen.movieList.view

import androidx.recyclerview.widget.DiffUtil
import com.dolgalyova.tmdbclient.screen.movieList.presentation.MovieListItem

class MovieListItemDiffCallback : DiffUtil.ItemCallback<MovieListItem>() {

    override fun areItemsTheSame(oldItem: MovieListItem, newItem: MovieListItem): Boolean {
        return (oldItem is MovieListItem.Movie && newItem is MovieListItem.Movie && oldItem.item.id == newItem.item.id) ||
                (oldItem is MovieListItem.LoadNextProgress && newItem is MovieListItem.LoadNextProgress) ||
                (oldItem is MovieListItem.InitialProgress && newItem is MovieListItem.LoadNextProgress)
    }

    override fun areContentsTheSame(oldItem: MovieListItem, newItem: MovieListItem) = oldItem == newItem
}