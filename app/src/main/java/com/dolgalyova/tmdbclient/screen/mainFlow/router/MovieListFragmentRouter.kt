package com.dolgalyova.tmdbclient.screen.mainFlow.router

import com.dolgalyova.tmdbclient.screen.movieList.data.model.MovieShort
import com.dolgalyova.tmdbclient.screen.movieList.router.MovieListRouter

class MovieListFragmentRouter(private val navigator: MainFlowNavigator) : MovieListRouter {

    override fun openMovie(movie: MovieShort) = navigator.openMovie(movie)
}