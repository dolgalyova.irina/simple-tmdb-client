package com.dolgalyova.tmdbclient.screen.host.di

import com.dolgalyova.tmdbclient.common.di.ActivityScope
import com.dolgalyova.tmdbclient.screen.host.MainViewModelFactory
import com.dolgalyova.tmdbclient.screen.host.router.AppNavigator
import com.dolgalyova.tmdbclient.screen.host.router.NavigationHost
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    @ActivityScope
    fun navigator(host: NavigationHost) = AppNavigator(host)

    @Provides
    @ActivityScope
    fun viewModelFactory(navigator: AppNavigator) = MainViewModelFactory(navigator)
}