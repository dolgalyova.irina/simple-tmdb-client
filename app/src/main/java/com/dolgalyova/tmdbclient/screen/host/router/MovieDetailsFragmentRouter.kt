package com.dolgalyova.tmdbclient.screen.host.router

import com.dolgalyova.tmdbclient.screen.movieDetails.router.MovieDetailsRouter

class MovieDetailsFragmentRouter(private val navigator: AppNavigator) : MovieDetailsRouter {

    override fun exit() = navigator.goBack()
}