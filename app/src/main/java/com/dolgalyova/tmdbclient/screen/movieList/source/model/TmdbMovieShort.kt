package com.dolgalyova.tmdbclient.screen.movieList.source.model

data class TmdbMovieShort(val id: Int,
                          val title: String,
                          val poster_path: String?,
                          val backdrop_path: String?,
                          val release_date: String?)