package com.dolgalyova.tmdbclient.screen.movieDetails.presentation

import com.dolgalyova.tmdbclient.screen.movieDetails.data.model.Movie

sealed class MovieDetailsViewState {
    data class Data(val movie: Movie) : MovieDetailsViewState()
    object Loading : MovieDetailsViewState()
}