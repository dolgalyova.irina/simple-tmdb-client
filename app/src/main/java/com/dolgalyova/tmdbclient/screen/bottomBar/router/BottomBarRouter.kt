package com.dolgalyova.tmdbclient.screen.bottomBar.router

interface BottomBarRouter {

    fun openPopularMovies()

    fun openUpcomingMovies()

    fun openNowPlayingMovies()
}