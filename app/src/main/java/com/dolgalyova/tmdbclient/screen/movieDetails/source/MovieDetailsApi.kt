package com.dolgalyova.tmdbclient.screen.movieDetails.source

import com.dolgalyova.tmdbclient.screen.movieDetails.source.model.TmdbMovie
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieDetailsApi {

    @GET("movie/{movieId}")
    fun getMovie(@Path("movieId") id: Int): Single<TmdbMovie>
}