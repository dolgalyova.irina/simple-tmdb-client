package com.dolgalyova.tmdbclient.screen.mainFlow.domain

import com.dolgalyova.tmdbclient.common.arch.Interactor
import com.dolgalyova.tmdbclient.common.arch.RxWorkers
import com.dolgalyova.tmdbclient.common.util.composeWith
import io.reactivex.Single
import timber.log.Timber

class MainFlowInteractor(private val workers: RxWorkers) : Interactor() {

    fun getFirstScreen(onSuccess: (MainFlowInitialScreen) -> Unit) {
        addSubscription(Single.fromCallable { MainFlowInitialScreen.Upcoming }
                .composeWith(workers)
                .subscribe(onSuccess, Timber::e))
    }
}