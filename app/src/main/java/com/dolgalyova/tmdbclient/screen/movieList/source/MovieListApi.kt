package com.dolgalyova.tmdbclient.screen.movieList.source

import com.dolgalyova.tmdbclient.screen.movieList.source.model.TmdbMovieListResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieListApi {

    @GET("movie/popular")
    fun getPopular(@Query("page") page: Int): Single<TmdbMovieListResponse>

    @GET("movie/upcoming")
    fun getUpcoming(@Query("page") page: Int): Single<TmdbMovieListResponse>

    @GET("movie/now_playing")
    fun getNowPlaying(@Query("page") page: Int): Single<TmdbMovieListResponse>
}