package com.dolgalyova.tmdbclient.screen.mainFlow.di

import com.dolgalyova.tmdbclient.common.di.FlowScope
import com.dolgalyova.tmdbclient.screen.bottomBar.router.BottomBarRouter
import com.dolgalyova.tmdbclient.screen.mainFlow.router.BottomBarFragmentRouter
import com.dolgalyova.tmdbclient.screen.mainFlow.router.MainFlowNavigator
import com.dolgalyova.tmdbclient.screen.mainFlow.router.MovieListFragmentRouter
import com.dolgalyova.tmdbclient.screen.movieList.router.MovieListRouter
import dagger.Module
import dagger.Provides

@Module
class MainFlowNavigationModule {

    @Provides
    @FlowScope
    fun movieListRouter(navigator: MainFlowNavigator): MovieListRouter =
            MovieListFragmentRouter(navigator)

    @Provides
    @FlowScope
    fun bottomBarRouter(navigator: MainFlowNavigator): BottomBarRouter =
            BottomBarFragmentRouter(navigator)
}