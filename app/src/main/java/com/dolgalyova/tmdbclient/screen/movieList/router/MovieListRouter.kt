package com.dolgalyova.tmdbclient.screen.movieList.router

import com.dolgalyova.tmdbclient.screen.movieList.data.model.MovieShort

interface MovieListRouter {

    fun openMovie(movie: MovieShort)
}