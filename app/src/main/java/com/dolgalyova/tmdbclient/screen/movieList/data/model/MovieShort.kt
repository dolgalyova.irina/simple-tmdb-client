package com.dolgalyova.tmdbclient.screen.movieList.data.model

import java.util.*

data class MovieShort(val id: Int,
                      val posterUrl: String,
                      val backdropUrl: String,
                      val title: String,
                      val releaseDate: Date?)