package com.dolgalyova.tmdbclient.screen.movieList.source.model

data class TmdbMovieListResponse(val page: Int, val results: List<TmdbMovieShort>)