package com.dolgalyova.tmdbclient.screen.movieList.di

import com.dolgalyova.tmdbclient.common.di.ScreenScope
import com.dolgalyova.tmdbclient.screen.movieList.MovieListFragment
import com.dolgalyova.tmdbclient.screen.movieList.presentation.MovieListType
import dagger.BindsInstance
import dagger.Subcomponent

@ScreenScope
@Subcomponent(modules = [MovieListModule::class])
interface MovieListComponent {

    fun inject(target: MovieListFragment)

    @Subcomponent.Builder
    interface Builder {
        fun module(module: MovieListModule): Builder
        @BindsInstance fun type(type: MovieListType): Builder
        fun build(): MovieListComponent
    }

    interface ComponentProvider {
        fun provideMovieListComponent(type: MovieListType): MovieListComponent
    }
}