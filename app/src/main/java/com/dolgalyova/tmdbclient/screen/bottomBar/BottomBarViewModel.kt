package com.dolgalyova.tmdbclient.screen.bottomBar

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.tmdbclient.screen.bottomBar.router.BottomBarRouter

class BottomBarViewModel(private val router: BottomBarRouter) : ViewModel() {

    fun onPopularMovieClick() = router.openPopularMovies()

    fun onUpcomingMoviesClick() = router.openUpcomingMovies()

    fun onNowPlayingMoviesClick() = router.openNowPlayingMovies()
}

class BottomBarViewModelFactory(private val router: BottomBarRouter) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = BottomBarViewModel(router) as T
}