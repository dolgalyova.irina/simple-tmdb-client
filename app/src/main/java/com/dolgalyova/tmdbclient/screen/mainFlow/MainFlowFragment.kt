package com.dolgalyova.tmdbclient.screen.mainFlow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.dolgalyova.tmdbclient.R
import com.dolgalyova.tmdbclient.common.arch.OnBackPressListener
import com.dolgalyova.tmdbclient.screen.bottomBar.di.BottomBarComponent
import com.dolgalyova.tmdbclient.screen.bottomBar.di.BottomBarModule
import com.dolgalyova.tmdbclient.screen.mainFlow.di.MainFlowComponent
import com.dolgalyova.tmdbclient.screen.movieList.di.MovieListComponent
import com.dolgalyova.tmdbclient.screen.movieList.di.MovieListModule
import com.dolgalyova.tmdbclient.screen.movieList.presentation.MovieListType
import javax.inject.Inject

class MainFlowFragment : Fragment(), MovieListComponent.ComponentProvider,
        BottomBarComponent.ComponentProvider, OnBackPressListener {

    companion object {
        fun create() = MainFlowFragment()
    }

    private val component by lazy {
        (activity as MainFlowComponent.ComponentProvider).provideMainFlowComponent(this)
    }
    @Inject lateinit var viewModelFactory: MainFlowViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_flow_main, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ViewModelProviders.of(this, viewModelFactory).get(MainFlowViewModel::class.java)
    }

    override fun provideMovieListComponent(type: MovieListType): MovieListComponent {
        return component.plusMovieList()
                .module(MovieListModule())
                .type(type)
                .build()
    }

    override fun provideBottomBarComponent(): BottomBarComponent {
        return component.plusBottomBar()
                .module(BottomBarModule())
                .build()
    }

    override fun onBackPress(): Boolean = true
}