package com.dolgalyova.tmdbclient.screen.movieDetails.data

import com.dolgalyova.tmdbclient.common.data.configuration.TmdbConfigurationSource
import com.dolgalyova.tmdbclient.common.data.configuration.TmdbImageConfiguration
import com.dolgalyova.tmdbclient.common.util.fromTmdbDate
import com.dolgalyova.tmdbclient.screen.movieDetails.data.model.Movie
import com.dolgalyova.tmdbclient.screen.movieDetails.source.MovieDetailsApi
import com.dolgalyova.tmdbclient.screen.movieDetails.source.MovieDetailsLocalSource
import com.dolgalyova.tmdbclient.screen.movieDetails.source.model.TmdbMovie
import io.reactivex.Single
import io.reactivex.functions.BiFunction

class MovieDetailsLocalRestRepository(private val api: MovieDetailsApi,
                                      private val localSource: MovieDetailsLocalSource,
                                      private val configurationSource: TmdbConfigurationSource) : MovieDetailsRepository {

    override fun getMovie(id: Int): Single<Movie> {
        val cached = localSource.get(id)
        val networkRequest = Single.zip(api.getMovie(id),
                configurationSource.getConfiguration(),
                BiFunction<TmdbMovie, TmdbImageConfiguration, Movie> { movie, config -> movie.toMovie(config) })
                .doOnSuccess { localSource.put(it) }
        return cached?.let { Single.just(it) } ?: networkRequest
    }
}

private fun TmdbMovie.toMovie(config: TmdbImageConfiguration): Movie {
    val posterSize = config.poster_sizes.lastOrNull().orEmpty()
    val backdropSize = config.backdrop_sizes.lastOrNull().orEmpty()
    val posterUrl = "${config.base_url}$posterSize${this.poster_path}"
    val backdropUrl = "${config.base_url}$backdropSize${this.backdrop_path}"
    val releaseDate = this.release_date.fromTmdbDate()
    return Movie(id = this.id,
            title = this.title,
            backdropUrl = backdropUrl,
            posterUrl = posterUrl,
            overview = this.overview,
            releaseDate = releaseDate,
            tagline = this.tagline,
            status = this.status)
}