package com.dolgalyova.tmdbclient.screen.bottomBar.di

import com.dolgalyova.tmdbclient.common.di.ScreenScope
import com.dolgalyova.tmdbclient.screen.bottomBar.BottomBarViewModelFactory
import com.dolgalyova.tmdbclient.screen.bottomBar.router.BottomBarRouter
import dagger.Module
import dagger.Provides

@Module
class BottomBarModule {

    @Provides
    @ScreenScope
    fun viewModelFactory(router: BottomBarRouter) = BottomBarViewModelFactory(router)
}