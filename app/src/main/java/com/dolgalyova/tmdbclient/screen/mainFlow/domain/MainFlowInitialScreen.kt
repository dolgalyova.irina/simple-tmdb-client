package com.dolgalyova.tmdbclient.screen.mainFlow.domain

sealed class MainFlowInitialScreen {
    object Upcoming : MainFlowInitialScreen()
    object NowPlaying : MainFlowInitialScreen()
    object Popular : MainFlowInitialScreen()
}