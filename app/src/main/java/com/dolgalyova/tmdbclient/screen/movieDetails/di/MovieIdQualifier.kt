package com.dolgalyova.tmdbclient.screen.movieDetails.di

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class MovieIdQualifier