package com.dolgalyova.tmdbclient.screen.movieList.data

import com.dolgalyova.tmdbclient.screen.movieList.data.model.MovieShort
import io.reactivex.Single

interface MovieListRepository {

    fun getMovies(page: Int): Single<List<MovieShort>>
}