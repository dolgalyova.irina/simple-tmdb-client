package com.dolgalyova.tmdbclient.screen.mainFlow.di

import com.dolgalyova.tmdbclient.common.di.FlowScope
import com.dolgalyova.tmdbclient.screen.bottomBar.di.BottomBarComponent
import com.dolgalyova.tmdbclient.screen.mainFlow.MainFlowFragment
import com.dolgalyova.tmdbclient.screen.movieList.di.MovieListComponent
import dagger.BindsInstance
import dagger.Subcomponent

@FlowScope
@Subcomponent(modules = [MainFlowModule::class, MainFlowNavigationModule::class])
interface MainFlowComponent {

    fun inject(target: MainFlowFragment)

    fun plusMovieList(): MovieListComponent.Builder
    fun plusBottomBar(): BottomBarComponent.Builder

    @Subcomponent.Builder
    interface Builder {
        fun module(module: MainFlowModule): Builder
        fun navigationModule(module: MainFlowNavigationModule): Builder
        @BindsInstance fun target(target: MainFlowFragment): Builder
        fun build(): MainFlowComponent
    }

    interface ComponentProvider {
        fun provideMainFlowComponent(target: MainFlowFragment): MainFlowComponent
    }
}