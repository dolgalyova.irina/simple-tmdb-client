package com.dolgalyova.tmdbclient.screen.mainFlow

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.tmdbclient.screen.mainFlow.domain.MainFlowInitialScreen.*
import com.dolgalyova.tmdbclient.screen.mainFlow.domain.MainFlowInteractor
import com.dolgalyova.tmdbclient.screen.mainFlow.router.MainFlowNavigator
import com.dolgalyova.tmdbclient.screen.mainFlow.router.MainFlowNavigator.Screen.*

class MainFlowViewModel(private val interactor: MainFlowInteractor,
                        private val navigator: MainFlowNavigator) : ViewModel() {

    init {
        interactor.getFirstScreen {
            when (it) {
                Upcoming -> navigator.setScreen(UpcomingMovies)
                NowPlaying -> navigator.setScreen(NowPlayingMovies)
                Popular -> navigator.setScreen(PopularMovies)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        interactor.cancel()
    }
}

class MainFlowViewModelFactory(private val interactor: MainFlowInteractor,
                               private val navigator: MainFlowNavigator) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>) = MainFlowViewModel(interactor, navigator) as T
}