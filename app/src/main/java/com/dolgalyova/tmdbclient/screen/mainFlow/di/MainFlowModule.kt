package com.dolgalyova.tmdbclient.screen.mainFlow.di

import com.dolgalyova.tmdbclient.common.arch.RxWorkers
import com.dolgalyova.tmdbclient.common.di.FlowScope
import com.dolgalyova.tmdbclient.screen.mainFlow.MainFlowFragment
import com.dolgalyova.tmdbclient.screen.mainFlow.MainFlowViewModelFactory
import com.dolgalyova.tmdbclient.screen.mainFlow.domain.MainFlowInteractor
import com.dolgalyova.tmdbclient.screen.mainFlow.router.MainFlowRouter
import com.dolgalyova.tmdbclient.screen.mainFlow.router.MainFlowNavigator
import dagger.Module
import dagger.Provides

@Module
class MainFlowModule {

    @Provides
    @FlowScope
    fun navigator(router: MainFlowRouter, host: MainFlowFragment) = MainFlowNavigator(host, router)

    @Provides
    @FlowScope
    fun interactor(workers: RxWorkers) = MainFlowInteractor(workers)

    @Provides
    @FlowScope
    fun mainFlowViewModelFactory(interactor: MainFlowInteractor, navigator: MainFlowNavigator) =
            MainFlowViewModelFactory(interactor, navigator)
}