package com.dolgalyova.tmdbclient.screen.movieList.di

import com.dolgalyova.tmdbclient.common.arch.RxWorkers
import com.dolgalyova.tmdbclient.common.data.configuration.TmdbConfigurationSource
import com.dolgalyova.tmdbclient.common.di.ScreenScope
import com.dolgalyova.tmdbclient.common.util.createApiService
import com.dolgalyova.tmdbclient.screen.movieList.data.MovieListLocalRestRepository
import com.dolgalyova.tmdbclient.screen.movieList.data.MovieListRepository
import com.dolgalyova.tmdbclient.screen.movieList.domain.MovieListInteractor
import com.dolgalyova.tmdbclient.screen.movieList.presentation.MovieListType
import com.dolgalyova.tmdbclient.screen.movieList.presentation.MovieListViewModelFactory
import com.dolgalyova.tmdbclient.screen.movieList.router.MovieListRouter
import com.dolgalyova.tmdbclient.screen.movieList.source.MovieListApi
import com.dolgalyova.tmdbclient.screen.movieList.source.MovieListLocalSource
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient

@Module
class MovieListModule {

    @Provides
    @ScreenScope
    fun api(client: OkHttpClient): MovieListApi = createApiService(MovieListApi::class.java, client)

    @Provides
    @ScreenScope
    fun repository(api: MovieListApi, configSource: TmdbConfigurationSource,
                   localSource: MovieListLocalSource, movieListType: MovieListType): MovieListRepository {
        return MovieListLocalRestRepository(api, localSource, configSource, movieListType)
    }

    @Provides
    @ScreenScope
    fun interactor(repository: MovieListRepository, workers: RxWorkers) =
            MovieListInteractor(repository, workers)

    @Provides
    @ScreenScope
    fun viewModelFactory(interactor: MovieListInteractor, router: MovieListRouter) =
            MovieListViewModelFactory(interactor, router)
}