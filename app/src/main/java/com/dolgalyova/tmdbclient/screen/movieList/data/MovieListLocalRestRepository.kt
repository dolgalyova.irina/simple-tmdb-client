package com.dolgalyova.tmdbclient.screen.movieList.data

import com.dolgalyova.tmdbclient.common.data.configuration.TmdbConfigurationSource
import com.dolgalyova.tmdbclient.common.data.configuration.TmdbImageConfiguration
import com.dolgalyova.tmdbclient.common.util.fromTmdbDate
import com.dolgalyova.tmdbclient.screen.movieList.presentation.MovieListType
import com.dolgalyova.tmdbclient.screen.movieList.presentation.MovieListType.*
import com.dolgalyova.tmdbclient.screen.movieList.data.model.MovieShort
import com.dolgalyova.tmdbclient.screen.movieList.source.MovieListApi
import com.dolgalyova.tmdbclient.screen.movieList.source.MovieListLocalSource
import com.dolgalyova.tmdbclient.screen.movieList.source.model.TmdbMovieShort
import io.reactivex.Single

class MovieListLocalRestRepository(private val api: MovieListApi,
                                   private val localSource: MovieListLocalSource,
                                   private val configurationSource: TmdbConfigurationSource,
                                   private val movieListType: MovieListType) : MovieListRepository {

    override fun getMovies(page: Int): Single<List<MovieShort>> {
        val cached = localSource.get(movieListType, page)
        return if (cached.isEmpty()) {
            val query = when (movieListType) {
                Popular -> api.getPopular(page)
                Upcoming -> api.getUpcoming(page)
                NowPlaying -> api.getNowPlaying(page)
            }
            configurationSource.getConfiguration().flatMap { config ->
                query.map { resp -> resp.results.map { it.toMovie(config) } }
            }.doOnSuccess { localSource.put(movieListType, page, it) }
        } else {
            Single.just(cached)
        }
    }
}

private fun TmdbMovieShort.toMovie(config: TmdbImageConfiguration): MovieShort {
    val posterSize = config.poster_sizes.lastOrNull().orEmpty()
    val backdropSize = config.backdrop_sizes.lastOrNull().orEmpty()
    val posterUrl = "${config.base_url}$posterSize${this.poster_path}"
    val backdropUrl = "${config.base_url}$backdropSize${this.backdrop_path}"
    val releaseDate = this.release_date.orEmpty()
    return MovieShort(
        id = this.id,
        posterUrl = posterUrl,
        backdropUrl = backdropUrl,
        title = this.title,
        releaseDate = releaseDate.fromTmdbDate())
}