package com.dolgalyova.tmdbclient.screen.movieDetails.di

import com.dolgalyova.tmdbclient.common.arch.RxWorkers
import com.dolgalyova.tmdbclient.common.data.configuration.TmdbConfigurationSource
import com.dolgalyova.tmdbclient.common.di.FlowScope
import com.dolgalyova.tmdbclient.common.util.createApiService
import com.dolgalyova.tmdbclient.screen.movieDetails.data.MovieDetailsLocalRestRepository
import com.dolgalyova.tmdbclient.screen.movieDetails.data.MovieDetailsRepository
import com.dolgalyova.tmdbclient.screen.movieDetails.domain.MovieDetailsInteractor
import com.dolgalyova.tmdbclient.screen.movieDetails.domain.MovieId
import com.dolgalyova.tmdbclient.screen.movieDetails.presentation.MovieDetailsViewModelFactory
import com.dolgalyova.tmdbclient.screen.movieDetails.router.MovieDetailsRouter
import com.dolgalyova.tmdbclient.screen.movieDetails.source.MovieDetailsApi
import com.dolgalyova.tmdbclient.screen.movieDetails.source.MovieDetailsLocalSource
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient

@Module
class MovieDetailsModule {

    @Provides
    @FlowScope
    fun api(client: OkHttpClient) = createApiService(MovieDetailsApi::class.java, client)

    @Provides
    @FlowScope
    fun repository(api: MovieDetailsApi, localSource: MovieDetailsLocalSource,
                   configurationSource: TmdbConfigurationSource): MovieDetailsRepository {
        return MovieDetailsLocalRestRepository(api, localSource, configurationSource)
    }

    @Provides
    @FlowScope
    fun interactor(repository: MovieDetailsRepository, @MovieIdQualifier movieId: Int, workers: RxWorkers) =
            MovieDetailsInteractor(repository, MovieId(movieId), workers)

    @Provides
    @FlowScope
    fun viewModelFactory(interactor: MovieDetailsInteractor, router: MovieDetailsRouter) =
            MovieDetailsViewModelFactory(interactor, router)
}