package com.dolgalyova.tmdbclient.screen.bottomBar.di

import com.dolgalyova.tmdbclient.common.di.ScreenScope
import com.dolgalyova.tmdbclient.screen.bottomBar.BottomBarFragment
import dagger.Subcomponent

@ScreenScope
@Subcomponent(modules = [BottomBarModule::class])
interface BottomBarComponent {

    fun inject(target: BottomBarFragment)

    @Subcomponent.Builder
    interface Builder {
        fun module(module: BottomBarModule): Builder
        fun build(): BottomBarComponent
    }

    interface ComponentProvider {
        fun provideBottomBarComponent(): BottomBarComponent
    }
}