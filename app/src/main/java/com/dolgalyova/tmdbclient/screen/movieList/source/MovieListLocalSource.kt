package com.dolgalyova.tmdbclient.screen.movieList.source

import com.dolgalyova.tmdbclient.screen.movieList.presentation.MovieListType
import com.dolgalyova.tmdbclient.screen.movieList.data.model.MovieShort
import java.util.concurrent.ConcurrentHashMap

class MovieListLocalSource {
    private val cache = ConcurrentHashMap<CacheKey, List<MovieShort>>()

    fun put(type: MovieListType, page: Int, data: List<MovieShort>) {
        cache[CacheKey(type, page)] = data
    }

    fun get(type: MovieListType, page: Int): List<MovieShort> {
        val key = CacheKey(type, page)
        return cache[key].orEmpty()
    }
}

private data class CacheKey(val type: MovieListType, val page: Int)