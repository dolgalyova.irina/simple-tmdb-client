package com.dolgalyova.tmdbclient.screen.movieList.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dolgalyova.tmdbclient.R
import com.dolgalyova.tmdbclient.common.util.setImageUrl
import com.dolgalyova.tmdbclient.common.util.toDateString
import com.dolgalyova.tmdbclient.screen.movieList.data.model.MovieShort
import com.dolgalyova.tmdbclient.screen.movieList.presentation.MovieListItem
import kotlinx.android.synthetic.main.item_movie_list_movie.view.*

class MovieListAdapter(private val onItemClick: (MovieShort) -> Unit) :
        ListAdapter<MovieListItem, RecyclerView.ViewHolder>(MovieListItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(viewType, parent, false)
        return when (viewType) {
            R.layout.item_movie_list_movie -> MovieHolder(itemView, onItemClick)
            else -> ProgressHolder(itemView)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        if (item is MovieListItem.Movie) {
            val movie = item.item
            (holder as? MovieHolder)?.movie = movie
            with(holder.itemView) {
                movieTitle.text = movie.title
                movieSubtitle.text = movie.releaseDate.toDateString()
                moviePoster.setImageUrl(movie.posterUrl)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return when (item) {
            is MovieListItem.Movie -> R.layout.item_movie_list_movie
            is MovieListItem.InitialProgress -> R.layout.item_movie_list_initial_progress
            is MovieListItem.LoadNextProgress -> R.layout.item_movie_list_load_more_progress
        }
    }

    class MovieHolder(itemView: View, onItemClick: (MovieShort) -> Unit) : RecyclerView.ViewHolder(itemView) {
        internal var movie: MovieShort? = null

        init {
            itemView.setOnClickListener { _ -> movie?.let { onItemClick(it) } }
        }
    }

    class ProgressHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}