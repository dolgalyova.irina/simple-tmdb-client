package com.dolgalyova.tmdbclient.screen.host.di

import com.dolgalyova.tmdbclient.app.di.AppComponent
import com.dolgalyova.tmdbclient.common.di.ActivityScope
import com.dolgalyova.tmdbclient.screen.host.MainActivity
import com.dolgalyova.tmdbclient.screen.mainFlow.di.MainFlowComponent
import com.dolgalyova.tmdbclient.screen.movieDetails.di.MovieDetailsComponent
import dagger.BindsInstance
import dagger.Component

@ActivityScope
@Component(dependencies = [AppComponent::class], modules = [MainModule::class, MainNavigationModule::class])
interface MainComponent {

    fun inject(target: MainActivity)

    fun plusMainFlow(): MainFlowComponent.Builder
    fun plusMovieDetailsFlow(): MovieDetailsComponent.Builder

    @Component.Builder
    interface Builder {
        fun parent(parent: AppComponent): Builder
        fun module(module: MainModule): Builder
        fun navigationModule(navigationModule: MainNavigationModule): Builder
        @BindsInstance fun target(target: MainActivity): Builder
        fun build(): MainComponent
    }
}