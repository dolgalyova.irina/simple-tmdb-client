package com.dolgalyova.tmdbclient.screen.host.router

import com.dolgalyova.tmdbclient.screen.mainFlow.router.MainFlowRouter
import com.dolgalyova.tmdbclient.screen.movieList.data.model.MovieShort

class MainFlowFragmentRouter(private val navigator: AppNavigator) : MainFlowRouter {

    override fun openMovie(movie: MovieShort) = navigator.setFlow(Flow.MovieDetails, movie)

    override fun goBack() = navigator.goBack()
}