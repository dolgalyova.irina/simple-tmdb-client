package com.dolgalyova.tmdbclient.screen.movieList.domain

import com.dolgalyova.tmdbclient.common.arch.Interactor
import com.dolgalyova.tmdbclient.common.arch.RxWorkers
import com.dolgalyova.tmdbclient.common.util.composeWith
import com.dolgalyova.tmdbclient.screen.movieList.data.MovieListRepository
import com.dolgalyova.tmdbclient.screen.movieList.data.model.MovieShort

class MovieListInteractor(private val repository: MovieListRepository,
                          private val workers: RxWorkers) : Interactor() {

    fun getMovies(page: Int, onSuccess: (List<MovieShort>) -> Unit, onError: (Throwable) -> Unit) {
        addSubscription(repository.getMovies(page)
            .composeWith(workers)
            .subscribe(onSuccess, onError))
    }
}