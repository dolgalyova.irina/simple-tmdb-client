package com.dolgalyova.tmdbclient.screen.movieDetails.data.model

import java.util.*

data class Movie(val id: Int,
                 val title: String,
                 val backdropUrl: String,
                 val posterUrl: String,
                 val overview: String,
                 val releaseDate: Date?,
                 val tagline: String,
                 val status: String)