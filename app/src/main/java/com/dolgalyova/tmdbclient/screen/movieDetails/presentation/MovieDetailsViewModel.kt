package com.dolgalyova.tmdbclient.screen.movieDetails.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.tmdbclient.common.arch.SingleLiveEvent
import com.dolgalyova.tmdbclient.screen.movieDetails.data.model.Movie
import com.dolgalyova.tmdbclient.screen.movieDetails.domain.MovieDetailsInteractor
import com.dolgalyova.tmdbclient.screen.movieDetails.router.MovieDetailsRouter
import timber.log.Timber

class MovieDetailsViewModel(private val interactor: MovieDetailsInteractor,
                            private val router: MovieDetailsRouter) : ViewModel() {
    private val state = MutableLiveData<MovieDetailsViewState>()
    val stateObservable: LiveData<MovieDetailsViewState>
        get() = state
    private val errors = SingleLiveEvent<Any>()
    val errorsObservable: LiveData<Any>
        get() = errors

    init {
        state.value = MovieDetailsViewState.Loading
        interactor.getMovie(::onDataAvailable, ::onError)
    }

    override fun onCleared() {
        super.onCleared()
        interactor.cancel()
    }

    fun onBackClick() = router.exit()

    private fun onDataAvailable(movie: Movie) {
        state.value = MovieDetailsViewState.Data(movie)
    }

    private fun onError(error: Throwable) {
        Timber.e(error)
        errors.call()
    }
}

class MovieDetailsViewModelFactory(private val interactor: MovieDetailsInteractor,
                                   private val router: MovieDetailsRouter) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T = MovieDetailsViewModel(interactor, router) as T
}