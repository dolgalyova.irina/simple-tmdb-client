package com.dolgalyova.tmdbclient.screen.movieDetails.domain

import com.dolgalyova.tmdbclient.common.arch.Interactor
import com.dolgalyova.tmdbclient.common.arch.RxWorkers
import com.dolgalyova.tmdbclient.common.util.composeWith
import com.dolgalyova.tmdbclient.screen.movieDetails.data.MovieDetailsRepository
import com.dolgalyova.tmdbclient.screen.movieDetails.data.model.Movie

class MovieDetailsInteractor(private val repository: MovieDetailsRepository, private val movieId: MovieId,
                             private val workers: RxWorkers) : Interactor() {

    fun getMovie(onSuccess: (Movie) -> Unit, onError: (Throwable) -> Unit) {
        addSubscription(repository.getMovie(movieId.id)
                .composeWith(workers)
                .subscribe(onSuccess, onError))
    }
}

inline class MovieId(val id: Int)