package com.dolgalyova.tmdbclient.screen.movieDetails.source

import com.dolgalyova.tmdbclient.screen.movieDetails.data.model.Movie
import java.util.concurrent.ConcurrentHashMap

class MovieDetailsLocalSource {
    private val cache = ConcurrentHashMap<Int, Movie>()

    fun put(movie: Movie) {
        cache[movie.id] = movie
    }

    fun get(id: Int): Movie? = cache[id]
}