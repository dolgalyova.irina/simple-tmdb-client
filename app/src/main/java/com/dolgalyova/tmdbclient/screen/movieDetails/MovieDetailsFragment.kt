package com.dolgalyova.tmdbclient.screen.movieDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dolgalyova.tmdbclient.BuildConfig
import com.dolgalyova.tmdbclient.R
import com.dolgalyova.tmdbclient.common.arch.OnBackPressListener
import com.dolgalyova.tmdbclient.common.util.setImageUrl
import com.dolgalyova.tmdbclient.common.util.showGeneralError
import com.dolgalyova.tmdbclient.screen.movieDetails.data.model.Movie
import com.dolgalyova.tmdbclient.screen.movieDetails.di.MovieDetailsComponent
import com.dolgalyova.tmdbclient.screen.movieDetails.domain.MovieId
import com.dolgalyova.tmdbclient.screen.movieDetails.presentation.MovieDetailsViewModel
import com.dolgalyova.tmdbclient.screen.movieDetails.presentation.MovieDetailsViewModelFactory
import com.dolgalyova.tmdbclient.screen.movieDetails.presentation.MovieDetailsViewState
import kotlinx.android.synthetic.main.fragment_movie_details.*
import javax.inject.Inject

class MovieDetailsFragment : Fragment(), OnBackPressListener {

    companion object {
        private const val EXTRA_MOVIE_ID = "${BuildConfig.APPLICATION_ID}.EXTRA_MOVIE_ID"

        fun create(movieId: Int): Fragment {
            val args = Bundle().apply { putInt(EXTRA_MOVIE_ID, movieId) }
            return MovieDetailsFragment().apply { arguments = args }
        }
    }

    private val component by lazy {
        val movieId = MovieId(arguments?.getInt(EXTRA_MOVIE_ID, -1) ?: -1)
        (activity as MovieDetailsComponent.ComponentProvider).provideMovieDetailsComponent(movieId)
    }

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MovieDetailsViewModel::class.java)
    }
    @Inject lateinit var viewModelFactory: MovieDetailsViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_movie_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.stateObservable.observe(this, Observer(::renderState))
        viewModel.errorsObservable.observe(this, Observer { showGeneralError() })
    }

    override fun onBackPress(): Boolean {
        viewModel.onBackClick()
        return true
    }

    private fun renderState(state: MovieDetailsViewState) = when (state) {
        is MovieDetailsViewState.Data -> showContent(state.movie)
        is MovieDetailsViewState.Loading -> showLoading()
    }

    private fun showContent(content: Movie) {
        collapsingToolbar.title = content.title
        backdrop.setImageUrl(content.backdropUrl)
        movieOverview.text = content.overview
        movieTagline.text = content.tagline
    }

    private fun showLoading() {

    }
}