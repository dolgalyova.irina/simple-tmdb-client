package com.dolgalyova.tmdbclient.screen.bottomBar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.dolgalyova.tmdbclient.R
import com.dolgalyova.tmdbclient.screen.bottomBar.di.BottomBarComponent
import kotlinx.android.synthetic.main.fragment_bottom_bar.*
import javax.inject.Inject

class BottomBarFragment : Fragment() {

    companion object {
        fun create() = BottomBarFragment()
    }

    private val component by lazy {
        (parentFragment as BottomBarComponent.ComponentProvider).provideBottomBarComponent()
    }
    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(BottomBarViewModel::class.java)
    }
    @Inject lateinit var viewModelFactory: BottomBarViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_bottom_bar, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.upcoming -> viewModel.onUpcomingMoviesClick()
                R.id.nowPlaying -> viewModel.onNowPlayingMoviesClick()
                R.id.popular -> viewModel.onPopularMovieClick()
            }
            true
        }
    }
}