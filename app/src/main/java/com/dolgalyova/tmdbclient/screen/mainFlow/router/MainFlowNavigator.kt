package com.dolgalyova.tmdbclient.screen.mainFlow.router

import androidx.annotation.IdRes
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.dolgalyova.tmdbclient.R
import com.dolgalyova.tmdbclient.common.util.canHandleNavigation
import com.dolgalyova.tmdbclient.common.util.clearBackStack
import com.dolgalyova.tmdbclient.screen.bottomBar.BottomBarFragment
import com.dolgalyova.tmdbclient.screen.mainFlow.MainFlowFragment
import com.dolgalyova.tmdbclient.screen.mainFlow.router.MainFlowNavigator.Screen.*
import com.dolgalyova.tmdbclient.screen.movieList.MovieListFragment
import com.dolgalyova.tmdbclient.screen.movieList.data.model.MovieShort
import com.dolgalyova.tmdbclient.screen.movieList.presentation.MovieListType
import kotlinx.android.synthetic.main.fragment_flow_main.*

class MainFlowNavigator(private val host: MainFlowFragment,
                        private val router: MainFlowRouter) {
    private var pendingScreen: Pair<Screen, Any?>? = null

    fun setScreen(screen: Screen, data: Any? = null) {
        if (host.canHandleNavigation()) {
            val fragment: Fragment? = when (screen) {
                PopularMovies -> MovieListFragment.create(MovieListType.Popular)
                NowPlayingMovies -> MovieListFragment.create(MovieListType.NowPlaying)
                UpcomingMovies -> MovieListFragment.create(MovieListType.Upcoming)
            }
            val addToBackStack = false
            val shouldShowBottomBar = true
            fragment?.let { setFragment(it, addToBackStack) }

            if (shouldShowBottomBar) showBottomBar()
            else hideBottomBar()
        } else {
            pendingScreen = screen to data
            attachObserver()
        }
    }

    fun openMovie(movie: MovieShort) = router.openMovie(movie)

    fun goBack() {
        if (host.canHandleNavigation()) {
            val fm = host.childFragmentManager
            if (fm.backStackEntryCount > 0) fm.popBackStack()
            else router.goBack()
        }
    }

    private fun setFragment(child: Fragment, addToBackStack: Boolean, @IdRes containerId: Int = R.id.mainFlowContainer) {
        val fm = host.childFragmentManager
        val transaction = fm
                .beginTransaction()
                .replace(containerId, child, child::class.java.simpleName)

        if (addToBackStack) transaction.addToBackStack(null)
        else fm.clearBackStack()

        transaction.commit()
    }

    private fun showBottomBar() {
        if (host.canHandleNavigation()) {
            val containsFragment = host.childFragmentManager.fragments
                    .any { it is BottomBarFragment }
            host.bottomBarContainer.isVisible = true
            if (!containsFragment) setFragment(BottomBarFragment.create(), false, R.id.bottomBarContainer)
        }
    }

    private fun hideBottomBar() {
        if (host.canHandleNavigation()) host.bottomBarContainer.isVisible = false
    }

    private fun attachObserver() {
        host.lifecycle.addObserver(object : LifecycleObserver {

            @OnLifecycleEvent(Lifecycle.Event.ON_START)
            fun dispatchPendingNavigation() {
                pendingScreen?.let { setScreen(it.first, it.second) }
                pendingScreen = null
                host.lifecycle.removeObserver(this)
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun clearObserver() = host.lifecycle.removeObserver(this)
        })
    }

    enum class Screen {
        PopularMovies, NowPlayingMovies, UpcomingMovies
    }
}