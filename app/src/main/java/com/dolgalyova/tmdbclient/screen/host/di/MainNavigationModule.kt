package com.dolgalyova.tmdbclient.screen.host.di

import com.dolgalyova.tmdbclient.common.di.ActivityScope
import com.dolgalyova.tmdbclient.screen.host.router.AppNavigator
import com.dolgalyova.tmdbclient.screen.host.router.MainFlowFragmentRouter
import com.dolgalyova.tmdbclient.screen.host.router.MovieDetailsFragmentRouter
import com.dolgalyova.tmdbclient.screen.mainFlow.router.MainFlowRouter
import com.dolgalyova.tmdbclient.screen.movieDetails.router.MovieDetailsRouter
import dagger.Module
import dagger.Provides

@Module
class MainNavigationModule {

    @Provides
    @ActivityScope
    fun mainFlowRouter(navigator: AppNavigator): MainFlowRouter = MainFlowFragmentRouter(navigator)

    @Provides
    @ActivityScope
    fun movieDetailsRouter(navigator: AppNavigator): MovieDetailsRouter = MovieDetailsFragmentRouter(navigator)
}