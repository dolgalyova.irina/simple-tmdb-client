package com.dolgalyova.tmdbclient.screen.host

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.dolgalyova.tmdbclient.R
import com.dolgalyova.tmdbclient.app.App
import com.dolgalyova.tmdbclient.common.arch.OnBackPressListener
import com.dolgalyova.tmdbclient.screen.host.di.DaggerMainComponent
import com.dolgalyova.tmdbclient.screen.host.di.MainModule
import com.dolgalyova.tmdbclient.screen.host.di.MainNavigationModule
import com.dolgalyova.tmdbclient.screen.host.router.NavigationHost
import com.dolgalyova.tmdbclient.screen.mainFlow.MainFlowFragment
import com.dolgalyova.tmdbclient.screen.mainFlow.di.MainFlowComponent
import com.dolgalyova.tmdbclient.screen.mainFlow.di.MainFlowModule
import com.dolgalyova.tmdbclient.screen.mainFlow.di.MainFlowNavigationModule
import com.dolgalyova.tmdbclient.screen.movieDetails.di.MovieDetailsComponent
import com.dolgalyova.tmdbclient.screen.movieDetails.di.MovieDetailsModule
import com.dolgalyova.tmdbclient.screen.movieDetails.domain.MovieId
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainFlowComponent.ComponentProvider, MovieDetailsComponent.ComponentProvider {
    private val component by lazy {
        DaggerMainComponent.builder()
                .parent((application as App).component)
                .module(MainModule())
                .navigationModule(MainNavigationModule())
                .target(this)
                .build()
    }

    @Inject lateinit var viewModelFactory: MainViewModelFactory
    @Inject lateinit var navigationHost: NavigationHost

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        component.inject(this)
        ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        navigationHost.attachHost(this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val topFragment = supportFragmentManager.fragments.firstOrNull { it is OnBackPressListener }
        val processed = if (topFragment?.isVisible == true) {
            (topFragment as? OnBackPressListener)?.onBackPress() ?: false
        } else false
        if (!processed) super.onBackPressed()
    }

    override fun onStop() {
        super.onStop()
        navigationHost.detachHost()
    }

    override fun provideMainFlowComponent(target: MainFlowFragment): MainFlowComponent {
        return component.plusMainFlow()
                .module(MainFlowModule())
                .navigationModule(MainFlowNavigationModule())
                .target(target)
                .build()
    }

    override fun provideMovieDetailsComponent(id: MovieId): MovieDetailsComponent {
        return component.plusMovieDetailsFlow()
                .module(MovieDetailsModule())
                .movieId(id.id)
                .build()
    }
}