package com.dolgalyova.tmdbclient.screen.mainFlow.router

import com.dolgalyova.tmdbclient.screen.bottomBar.router.BottomBarRouter
import com.dolgalyova.tmdbclient.screen.mainFlow.router.MainFlowNavigator.Screen.*

class BottomBarFragmentRouter(private val navigator: MainFlowNavigator) : BottomBarRouter {

    override fun openPopularMovies() = navigator.setScreen(PopularMovies)

    override fun openUpcomingMovies() = navigator.setScreen(UpcomingMovies)

    override fun openNowPlayingMovies() = navigator.setScreen(NowPlayingMovies)
}