package com.dolgalyova.tmdbclient.screen.movieDetails.source.model

data class TmdbMovie(val id: Int,
                     val title: String,
                     val backdrop_path: String,
                     val poster_path: String,
                     val overview: String,
                     val release_date: String,
                     val tagline: String,
                     val status: String)