package com.dolgalyova.tmdbclient.common.arch

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class Interactor {
    private val subscriptions = CompositeDisposable()

    fun addSubscription(subscription: Disposable) = subscriptions.add(subscription)

    fun cancel() = subscriptions.dispose()
}