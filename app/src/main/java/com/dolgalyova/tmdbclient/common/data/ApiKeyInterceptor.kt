package com.dolgalyova.tmdbclient.common.data

import com.dolgalyova.tmdbclient.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class ApiKeyInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val originalUrl = original.url()
        val interceptedUrl = originalUrl.newBuilder()
            .addQueryParameter("api_key", BuildConfig.API_KEY)
            .build()
        val interceptedRequest = original.newBuilder()
            .url(interceptedUrl)
            .build()
        return chain.proceed(interceptedRequest)
    }
}