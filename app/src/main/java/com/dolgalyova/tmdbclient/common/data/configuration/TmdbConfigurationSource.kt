package com.dolgalyova.tmdbclient.common.data.configuration

import io.reactivex.Single

class TmdbConfigurationSource(private val api: TmdbConfigurationApi) {
    private var configuration: TmdbImageConfiguration? = null

    fun getConfiguration(): Single<TmdbImageConfiguration> {
        val apiRequest = api
            .getConfiguration()
            .map { it.images }
            .doOnSuccess { configuration = it }

        return Single.defer { configuration?.let { Single.just(it) } ?: apiRequest }
    }
}