package com.dolgalyova.tmdbclient.common.arch

import io.reactivex.Scheduler

data class RxWorkers(
    val subscribeWorker: Scheduler,
    val observeWorker: Scheduler
)