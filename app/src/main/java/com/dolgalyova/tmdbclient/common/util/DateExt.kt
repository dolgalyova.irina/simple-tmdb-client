package com.dolgalyova.tmdbclient.common.util

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

fun String.fromTmdbDate(): Date? {
    val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    return getOrNull { formatter.parse(this) }
}

fun Date?.toDateString(): String {
    val formatter = SimpleDateFormat.getDateInstance(DateFormat.LONG)
    return this?.let { formatter.format(it) }.orEmpty()
}