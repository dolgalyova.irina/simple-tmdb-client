package com.dolgalyova.tmdbclient.common.util

import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.dolgalyova.tmdbclient.R
import com.dolgalyova.tmdbclient.app.GlideApp
import com.dolgalyova.tmdbclient.common.view.LoadMoreScrollListener

fun ImageView.setImageUrl(url: String) {
    GlideApp.with(this)
            .load(url)
            .centerCrop()
            .error(R.color.colorPrimaryDark)
            .placeholder(R.color.colorPrimaryDark)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)
}

fun RecyclerView.setLoadMoreListener(onLoadNext: () -> Unit) {
    this.addOnScrollListener(LoadMoreScrollListener(onLoadNext))
}