package com.dolgalyova.tmdbclient.common.data.configuration

data class TmdbConfiguration(val images: TmdbImageConfiguration)

data class TmdbImageConfiguration(val base_url: String,
                                  val secure_base_url: String,
                                  val poster_sizes: List<String>,
                                  val backdrop_sizes: List<String>)