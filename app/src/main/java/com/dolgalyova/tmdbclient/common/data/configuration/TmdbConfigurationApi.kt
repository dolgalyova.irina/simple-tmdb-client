package com.dolgalyova.tmdbclient.common.data.configuration

import io.reactivex.Single
import retrofit2.http.GET

interface TmdbConfigurationApi {

    @GET("configuration")
    fun getConfiguration(): Single<TmdbConfiguration>
}