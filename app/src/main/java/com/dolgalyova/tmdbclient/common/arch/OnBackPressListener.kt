package com.dolgalyova.tmdbclient.common.arch

interface OnBackPressListener {
    fun onBackPress(): Boolean
}