package com.dolgalyova.tmdbclient.common.view

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class LoadMoreScrollListener(private val onLoadNext: () -> Unit, private val threshold: Int = 10)
    : RecyclerView.OnScrollListener() {

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        val layoutManager = recyclerView.layoutManager as? LinearLayoutManager
        layoutManager?.let {
            val lastVisible = it.findLastVisibleItemPosition()
            val count = recyclerView.adapter?.itemCount ?: 0
            if (lastVisible >= count - threshold || count == 0) {
                onLoadNext()
            }
        }
    }
}