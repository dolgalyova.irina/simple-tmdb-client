package com.dolgalyova.tmdbclient.app.di

import android.content.Context
import com.dolgalyova.tmdbclient.app.App
import com.dolgalyova.tmdbclient.common.arch.RxWorkers
import com.dolgalyova.tmdbclient.common.data.ApiKeyInterceptor
import com.dolgalyova.tmdbclient.common.data.configuration.TmdbConfigurationApi
import com.dolgalyova.tmdbclient.common.data.configuration.TmdbConfigurationSource
import com.dolgalyova.tmdbclient.common.util.createApiService
import com.dolgalyova.tmdbclient.screen.host.router.AppNavigationHost
import com.dolgalyova.tmdbclient.screen.host.router.NavigationHost
import com.dolgalyova.tmdbclient.screen.movieDetails.source.MovieDetailsLocalSource
import com.dolgalyova.tmdbclient.screen.movieList.source.MovieListLocalSource
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun appContext(application: App): Context = application.applicationContext

    @Provides
    @Singleton
    fun navigationHost(): NavigationHost = AppNavigationHost()

    @Provides
    @Singleton
    fun client(): OkHttpClient {
        return OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .addInterceptor(ApiKeyInterceptor())
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()
    }

    @Provides
    @Singleton
    fun workers() = RxWorkers(Schedulers.io(), AndroidSchedulers.mainThread())

    @Provides
    @Singleton
    fun movieListLocalSource() = MovieListLocalSource()

    @Provides
    @Singleton
    fun movieDetailsLocalSource() = MovieDetailsLocalSource()

    @Provides
    @Singleton
    fun configurationApi(client: OkHttpClient) = createApiService(TmdbConfigurationApi::class.java, client)

    @Provides
    @Singleton
    fun configurationSource(api: TmdbConfigurationApi) = TmdbConfigurationSource(api)
}