package com.dolgalyova.tmdbclient.app.di

import com.dolgalyova.tmdbclient.app.App
import com.dolgalyova.tmdbclient.common.arch.RxWorkers
import com.dolgalyova.tmdbclient.common.data.configuration.TmdbConfigurationSource
import com.dolgalyova.tmdbclient.screen.host.router.NavigationHost
import com.dolgalyova.tmdbclient.screen.movieDetails.source.MovieDetailsLocalSource
import com.dolgalyova.tmdbclient.screen.movieList.source.MovieListLocalSource
import dagger.BindsInstance
import dagger.Component
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(target: App)

    fun provideNavigationHost(): NavigationHost
    fun provideConfigurationSource(): TmdbConfigurationSource
    fun provideMovieListLocalSource(): MovieListLocalSource
    fun provideMovieDetailsLocalSource(): MovieDetailsLocalSource
    fun provideOkHttpClient(): OkHttpClient
    fun provideWorkers(): RxWorkers

    @Component.Builder
    interface Builder {
        fun module(module: AppModule): Builder
        @BindsInstance fun target(target: App): Builder
        fun build(): AppComponent
    }
}