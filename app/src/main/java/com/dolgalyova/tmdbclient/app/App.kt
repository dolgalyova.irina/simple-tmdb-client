package com.dolgalyova.tmdbclient.app

import android.app.Application
import com.dolgalyova.tmdbclient.BuildConfig
import com.dolgalyova.tmdbclient.app.di.AppModule
import com.dolgalyova.tmdbclient.app.di.DaggerAppComponent
import timber.log.Timber

class App : Application() {
    val component by lazy {
        DaggerAppComponent.builder()
                .module(AppModule())
                .target(this)
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
    }
}